const boardData=require("./boards_1.json");
const callback1=require("./callback1.cjs");
const callback2=require("./callback2.cjs");
const callback3=require("./callback3.cjs");


function solve(callback) {
    setTimeout(() => {

        let thanosId=null;
        boardData.find((ele)=>{
            if(ele.name === "Thanos"){
                thanosId=ele.id;
            }
        })
        
        callback1(thanosId,(err,data)=>{
            if(err){
                console.log(err);
            }
            else{
                console.log(data);
                callback2(thanosId,(err,data)=>{
                    if(err){
                        console.log(err);
                    }
                    else{
                        console.log(data);

                        callback3(thanosId,(err,data)=>{
                            if(err){
                                console.log(err);
                            }
                            else{
                                console.log(data);
                            }
                        });
                    }
                });
            }
        });
        
        
        
    }, 2 * 1000);
}

module.exports = solve;