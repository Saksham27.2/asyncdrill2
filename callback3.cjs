const cardsData = require("./cards_1.json");


function handleCards(cards){

        if (cards) {
            console.log(cardsData[cards]);
        } else {
            console.log("No cards found for the given list");
        }
   

    }
function getCards(list  , callback){

        for (let keys in cardsData){
            if (list === keys) {
                callback(keys);
                return;
            }
        }
        callback(null);

}


function problem3(list, callback) {
    setTimeout(() => {
        getCards(list, callback);
    }, 1000)
}

module.exports= { 
    problem3,
    handleCards
}